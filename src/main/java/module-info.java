module com.example.projetlastfm {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    //requires jbcrypt;
    //requires org.json;
    requires java.xml;
    //requires mongo.java.driver;
    requires org.mongodb.bson;
    requires org.mongodb.driver.core;
    requires org.mongodb.driver.sync.client;


    opens com.example.projetlastfm to javafx.fxml;
    exports com.example.projetlastfm;
}