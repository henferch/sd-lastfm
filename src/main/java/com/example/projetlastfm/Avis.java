package com.example.projetlastfm;

import org.bson.*;

public class Avis {
    String nomObjet;
    String nomArtiste;
    String nomUser;
    int note;
    String commentaire;

    //si le type est faux, il s'agit d'un avis non fabriqué avec un document
    public Avis(Document doc , boolean type){
        if(type) {
            BsonDocument bsonDoc = doc.toBsonDocument();
            if(bsonDoc.get("nomObjet").getBsonType()!=BsonType.NULL){
                this.nomObjet = bsonDoc.get("nomObjet").asString().getValue();
            }
            if(bsonDoc.get("nomArtiste").getBsonType()!=BsonType.NULL) {
                this.nomArtiste = bsonDoc.get("nomArtiste").asString().getValue();
            }
            this.nomUser = bsonDoc.get("nomUser").asString().getValue();
            this.commentaire = bsonDoc.get("commentaire").asString().getValue();
            this.note = bsonDoc.get("note").asInt32().getValue();
        }
        else {
            this.nomObjet = null;
            this.nomArtiste = null;
            this.nomUser = null;
            this.note = 0;
            this.commentaire = null;
        }
    }

    public void updateAvis(String nomObjet, String nomArtiste, String nomUser, int note, String commentaire){
        this.nomObjet=nomObjet;
        this.nomArtiste=nomArtiste;
        this.nomUser=nomUser;
        this.note=note;
        this.commentaire=commentaire;
    }
}
