package com.example.projetlastfm;

import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class TopGenTrack {
    Map<String, Integer> musiquesEcoutes = new HashMap<>();
    Map<String, String> musiquesArtiste = new HashMap<>();

    public TopGenTrack(Document doc) {
        BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("tracks");
        BsonArray doc1 = bsonDoc.getArray("track");
        for (BsonValue value : bsonDoc.getArray("track")) {
            BsonDocument docValue = value.asDocument();
            this.musiquesEcoutes.put(docValue.get("name").asString().getValue(), Integer.valueOf(docValue.get("playcount").asString().getValue()));
            BsonDocument docArtiste = (BsonDocument) docValue.get("artist");
            this.musiquesArtiste.put(docValue.get("name").asString().getValue(), docArtiste.get("name").asString().getValue());
        }
    }

    public String toString(){
        String out="";
        String artiste="";
        for (Map.Entry<String, Integer> entry : this.musiquesEcoutes.entrySet()) {
            for (Map.Entry<String, String> art : this.musiquesArtiste.entrySet()) {
                if(art.getKey()==entry.getKey()){artiste=art.getValue();}
            }
            out+=(entry.getKey() + " ( "+artiste+" ) = " + entry.getValue() + " écoutes\n");
        }
        return out;
    }
}
