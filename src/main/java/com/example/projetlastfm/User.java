package com.example.projetlastfm;

import org.bson.BsonDocument;
import org.bson.Document;

public class User {
    public String pseudo;
    public String typeUser;

    public User(){
        this.pseudo=null;
        this.typeUser=null;
    }

    public void actuUser(Document doc){
        if(doc.isEmpty()){
            this.pseudo=null;
            this.typeUser=null;
        }
        else{
            BsonDocument bsonDoc = doc.toBsonDocument();
            this.pseudo=bsonDoc.get("name").asString().getValue();
            this.typeUser=bsonDoc.get("role").asString().getValue();
        }
    }

}
