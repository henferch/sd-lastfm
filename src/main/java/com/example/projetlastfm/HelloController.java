package com.example.projetlastfm;

import com.mongodb.client.MongoCollection;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import org.bson.Document;

import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import static com.mongodb.client.model.Filters.eq;

public class HelloController implements Initializable {
    User user = new User();
    String objetCourant=null;
    String artisteCourant=null;
    @FXML
    private Label welcomeText;
    @FXML
    private ChoiceBox inputTypeRecherche;
    @FXML
    private ChoiceBox inputTypeTop;
    @FXML
    private ChoiceBox connexion;
    @FXML
    private TextField inputRechercheAutre;
    @FXML
    private TextField inputRechercheArtiste;
    @FXML
    private TextField inputPays;
    @FXML
    private TextField inputPseudo;
    @FXML
    private TextField inputMDP;
    @FXML
    private TextField inputCommentaireAvis;
    @FXML
    private Slider inputNoteAvis;
    @FXML
    private VBox paneAvis;
    @FXML
    private Button buttonVoirAvisVal;

    public HelloController() throws UnknownHostException {
    }


//-----------------------------------------Initilisation-----------------------------------------

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        inputTypeRecherche.getItems().add("Artiste");
        inputTypeRecherche.getItems().add("Morceau");
        inputTypeRecherche.getItems().add("Album");
        inputTypeRecherche.getItems().add("Tag");
        inputTypeRecherche.setValue("Artiste");
        inputTypeTop.getItems().add("Tops Artiste");
        inputTypeTop.getItems().add("Tops Morceaux");
        inputTypeTop.getItems().add("Tops Tags");
        inputTypeTop.getItems().add("Tops Geo Artiste");
        inputTypeTop.getItems().add("Tops Geo Morceaux");
        inputTypeTop.setValue("Tops Artiste");
        connexion.getItems().add("Inscription");
        connexion.getItems().add("Connexion");
        connexion.getItems().add("Déconnexion");
        connexion.setValue("Connexion");
        paneAvis.setVisible(false);
        buttonVoirAvisVal.setVisible(false);
        welcomeText.setText("Veuillez d'abord vous connecter.");
    }

//--------------------------------------------Actions--------------------------------------------

    @FXML
    protected void onButtonConnexion(){
        try{
            AppelsBD appelsBD = new AppelsBD();
            switch (connexion.getValue().toString()){
                case "Connexion" :
                    if(user.pseudo==null){
                        if(appelsBD.connexion(user, inputPseudo.getText(), inputMDP.getText())){
                            paneAvis.setVisible(false);
                            welcomeText.setText("Bonjour " +user.pseudo);
                            inputMDP.setText("");
                            if(user.typeUser.equals("modo")){
                                buttonVoirAvisVal.setVisible(true);
                            }
                        }
                        else{welcomeText.setText("Erreur lors de la connexion. Réessayez.");}
                        break;
                    } else {
                        paneAvis.setVisible(false);
                        welcomeText.setText("Vous êtes déjà connecter. Veuillez vous déconnecter.");
                        break;
                    }
                case "Inscription" :
                    if(user.pseudo==null) {
                        appelsBD.inscription(user, inputPseudo.getText(), inputMDP.getText());
                        paneAvis.setVisible(false);
                        welcomeText.setText("Bienvenue "+user.pseudo);
                        break;
                    } else {
                        paneAvis.setVisible(false);
                        welcomeText.setText("Vous êtes déjà connecté. Veuillez vous déconnecter.");
                        break;
                    }
                default:
                    if(user.pseudo!=null) {
                        welcomeText.setText("Au revoir "+user.pseudo);
                        user.actuUser(new Document());
                        buttonVoirAvisVal.setVisible(false);
                    } else {
                        welcomeText.setText("Vous n'êtes pas connecté.");
                    }
                    paneAvis.setVisible(false);
            }
        }
        catch(Exception e){
            welcomeText.setText("Erreur rencontrée.");
            System.out.println(e);
        }
    }

    @FXML
    protected void onButtonSearch(){
        paneAvis.setVisible(false);
        try{
            if(user.typeUser!=null) {
                welcomeText.setText(affichageRecherche(inputRechercheAutre.getText(), inputRechercheArtiste.getText()));
            }
            else{welcomeText.setText("Veuillez vous connecter.");}
        }
        catch(Exception e){
            welcomeText.setText("Erreur rencontrée.");
            System.out.println(e);
        }
    }

    @FXML
    public void onButtonTopGeneraux(){
        paneAvis.setVisible(false);
        try{
            if(user.typeUser!=null) {
                welcomeText.setText(affichageTop(inputTypeTop.getValue().toString()));
            }
            else{welcomeText.setText("Veuillez vous connecter.");}
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    @FXML
    public void onButtonAvis(){
        AppelsBD appelsBD = new AppelsBD();
        try{
            Avis avis = new Avis(new Document(), false);
            avis.updateAvis(objetCourant, artisteCourant, user.pseudo, (int) inputNoteAvis.getValue(), inputCommentaireAvis.getText());
            appelsBD.addAvis(avis);
            if(objetCourant.equals(null)){welcomeText.setText("Avis sur "+artisteCourant+" enregistré");}
            else if(artisteCourant.equals(null)){welcomeText.setText("Avis sur " + objetCourant +" enregistré");}
            else{welcomeText.setText("Avis sur " + objetCourant + " (" + artisteCourant + ") enregistré");}
            objetCourant=null;
            artisteCourant=null;
        }catch(Exception e){
            welcomeText.setText("Avis non envoyé.");
        }
    }

    @FXML
    public void onButtonVoirAvis(){
        AppelsBD appelsBD = new AppelsBD();
        paneAvis.setVisible(false);
        Document avis = appelsBD.getAvisGen();
        welcomeText.setText(avis.toString());
    }

    public String affichageRecherche(String inputPrecis, String inputArtiste){
        artisteCourant=null;
        objetCourant=null;
        AppelsBD appelsBD = new AppelsBD();
        AppelsAPI appelsAPI = new AppelsAPI();
        switch (inputTypeRecherche.getValue().toString()) {
            case "Artiste":
                try {
                    if (appelsBD.rechercheArtiste(inputArtiste)) {
                        Artiste artiste = appelsBD.getArtiste(inputArtiste);
                        paneAvis.setVisible(true);
                        artisteCourant=artiste.nom;
                        artiste.addAvis(appelsBD.getAvis(null, artiste.nom));
                        return ((user.typeUser=="admin")?(artiste.toString()+"\n\n\n\nDONNEES EN LOCAL"):artiste.toString());
                    }
                    Artiste artiste = appelsAPI.getArtiste(inputArtiste);
                    appelsBD.addArtiste(artiste);
                    paneAvis.setVisible(true);
                    artisteCourant=artiste.nom;
                    return ((user.typeUser=="admin")?(artiste.toString()+"\n\n\n\nDONNEES API, MAJ BDD EFFECTUEE"):artiste.toString());
                } catch(Exception e){return "Erreur, Artiste non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            case "Morceau":
                try {
                    if (appelsBD.rechercheMorceau(inputPrecis, inputArtiste)) {
                        Morceau morceau = appelsBD.getMorceau(inputPrecis, inputArtiste);
                        morceau.addAvis(appelsBD.getAvis(morceau.artiste, morceau.nom));
                        paneAvis.setVisible(true);
                        artisteCourant=morceau.artiste;
                        objetCourant=morceau.nom;
                        return ((user.typeUser=="admin")?(morceau.toString()+"\n\n\n\nDONNEES EN LOCAL"):morceau.toString());
                    }
                    Morceau morceau = appelsAPI.getMorceau(inputPrecis, inputArtiste);
                    appelsBD.addMorceau(morceau);
                    paneAvis.setVisible(true);
                    artisteCourant=morceau.artiste;
                    objetCourant=morceau.nom;
                    return ((user.typeUser=="admin")?(morceau.toString()+"\n\n\n\nDONNEES API, MAJ BDD EFFECTUEE"):morceau.toString());
                } catch(Exception e){return "Erreur, Morceau non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            case "Album":
                try {
                    if (appelsBD.rechercheAlbum(inputPrecis, inputArtiste)) {
                        Album album = appelsBD.getAlbum(inputPrecis, inputArtiste);
                        paneAvis.setVisible(true);
                        artisteCourant=album.artiste;
                        objetCourant=album.nom;
                        album.addAvis(appelsBD.getAvis(album.nom, album.artiste));
                        return ((user.typeUser=="admin")?(album.toString()+"\n\n\n\nDONNEES EN LOCAL"):album.toString());
                    }
                    Album album = appelsAPI.getAlbum(inputPrecis, inputArtiste);
                    paneAvis.setVisible(true);
                    artisteCourant=album.artiste;
                    objetCourant=album.nom;
                    return ((user.typeUser=="admin")?(album.toString()+"\n\n\n\nDONNEES API, MAJ BDD EFFECTUEE"):album.toString());
                } catch(Exception e){return "Erreur, album non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            case "Tag":
                try {
                    if (appelsBD.rechercheTag(inputPrecis)) {
                        Tag tag = appelsBD.getTag(inputPrecis);
                        paneAvis.setVisible(true);
                        objetCourant=tag.nom;
                        tag.addAvis(appelsBD.getAvis(tag.nom, null));
                        return ((user.typeUser=="admin")?(tag.toString()+"\n\n\n\nDONNEES EN LOCAL"):tag.toString());
                    }
                    Tag tag = appelsAPI.getTag(inputPrecis);
                    appelsBD.addTag(tag);
                    paneAvis.setVisible(true);
                    objetCourant=tag.nom;
                    return ((user.typeUser=="admin")?(tag.toString()+"\n\n\n\nDONNEES API, MAJ BDD EFFECTUEE"):tag.toString());
                } catch(Exception e){return "Erreur, Tag non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            default:
                return "Veuillez renseigner un type de recherche.";
        }
    }

    public String affichageTop(String typeTop){
        AppelsAPI appelsAPI = new AppelsAPI();
        switch (typeTop){
            case "Tops Artiste" :
                try {
                    TopGenArtiste topGenArtist = appelsAPI.getTopGenArtiste();
                    return ((user.typeUser=="admin") ? (topGenArtist.toString()+"\n\nDonnées en ...") : topGenArtist.toString());
                } catch(Exception e){return "Erreur, top artistes non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            case "Tops Morceaux":
                try {
                    TopGenTrack topGenTrack = appelsAPI.getTopGenTrack();
                    return ((user.typeUser=="admin") ? (topGenTrack.toString()+"\n\nDonnées en ...") : topGenTrack.toString());
                } catch(Exception e){return "Erreur, top morceaux non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            case "Tops Tags":
                try {
                    TopGenTag topGenTag = appelsAPI.getTopGenTag();
                    return topGenTag.toString();
                } catch(Exception e){return "Erreur, top tags non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            case "Tops Geo Artiste" :
                try {
                    TopGeoArtist topGeoArtist = appelsAPI.getTopGeoArtist(inputPays.getText());
                    return topGeoArtist.toString();
                } catch(Exception e){return "Erreur, top geo artistes non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            case "Tops Geo Morceaux":
                try {
                    TopGeoTrack topGeoTrack = appelsAPI.getTopGeoTrack(inputPays.getText());
                    return topGeoTrack.toString();
                } catch(Exception e){return "Erreur, top geo morceaux non trouvé.\nVérifiez vos entrées et votre connexion internet.";}
            default: return "Veuillez renseigner un type de recherche et/ou un pays en anglais.";
        }
    }


}