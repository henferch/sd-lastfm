package com.example.projetlastfm;

import org.bson.BsonDocument;
import org.bson.Document;

import java.util.ArrayList;

public class Tag {
    public String nom;
    public int portee;
    public int nombre;
    public String resume;
    ArrayList<Avis> avis = new ArrayList<>();

    public Tag(Document doc , boolean typeCrea) {
        //Sil s'agit d'une création de tag via une requete API
        if(!typeCrea) {
            BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("tag");
            this.nom = bsonDoc.get("name").asString().getValue();
            this.portee = bsonDoc.get("total").asInt32().getValue();
            this.nombre = bsonDoc.get("reach").asInt32().getValue();
            BsonDocument description = (BsonDocument) bsonDoc.get("wiki");
            this.resume = description.get("summary").asString().getValue();
        }
        //sinon par une requete BDD
        else{
            BsonDocument bsonDoc = doc.toBsonDocument();
            this.nom = bsonDoc.get("nom").asString().getValue();
            this.portee = bsonDoc.get("portee").asInt32().getValue();
            this.nombre = bsonDoc.get("nombre").asInt32().getValue();
            this.resume = bsonDoc.get("resume").asString().getValue();
        }
    }


    public void addAvis(ArrayList<Document> doc){
        for(Document docUnique : doc){
            this.avis.add(new Avis(docUnique, true));
        }
    }

    @Override
    public String toString() {
        String sortieAvis="";
        for (Avis avis : this.avis) {
            sortieAvis+=avis.nomUser+" : "+avis.commentaire+" ( "+avis.note+"/10)\n";
        }
        return  " --- " + this.nom + " --- " +
                "\n\nPortée :" + this.portee +
                "\nNombre : " + this.nombre +
                "\n\n" + this.resume+"\n\n--------------Avis--------------\n" +sortieAvis;
    }
}
