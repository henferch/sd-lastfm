package com.example.projetlastfm;

import org.bson.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.logging.Logger;
//import org.json.JSONObject;
//import org.json.JSONException;

public class ConnectAPI {
    private static final Logger LOG = Logger.getLogger(ConnectAPI.class.getName());
    private String AppName ;
    private String APIKey;
    private String APIUrl;

    public ConnectAPI() {
        this.AppName = "structuration de doc";
        this.APIUrl = "http://ws.audioscrobbler.com/2.0/";
        this.APIKey = "b73a1d797a0edc873e0fc2c4264a3b71";
    }

    //test navigateur : http://ws.audioscrobbler.com/2.0/?method=artist.search&&artist=kikesa&api_key=b73a1d797a0edc873e0fc2c4264a3b71&format=json

    public Document connectionAPI(String methode, String parametres) {
        try {
            parametres = parametres.replace(" ", "+");
            URL url = new URL(APIUrl + "?method=" + methode + "&" + parametres + "&api_key=" + this.APIKey + "&format=json");
            HttpURLConnection connect = (HttpURLConnection) url.openConnection();
            connect.setRequestMethod("GET");
            connect.setRequestProperty("Accept", "application/json");
            System.out.println("coucou API");
            if (connect.getResponseCode() != 200) {
                LOG.warning("Erreur de retour ( != 200 )");
                return null;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder retour = new StringBuilder();
            String input;
            while ((input = in.readLine()) != null) {
                retour.append(input);
            }
            in.close();
            //JSONObject retourJSON = new JSONObject(stringToJSon(String.valueOf(retour)));
            return Document.parse(String.valueOf(retour));
        } catch (IOException e) {
            LOG.warning(e.getMessage());
        }
        return null;
    }



    public Document connectionAPI2(String methode, String parametres) {
        try {
            parametres = parametres.replace(" ", "+");
            URL url = new URL(APIUrl + "?method=" + methode + "&api_key=" + this.APIKey + "&" + parametres + "&format=json");
            HttpURLConnection connect = (HttpURLConnection) url.openConnection();
            connect.setRequestMethod("GET");
            connect.setRequestProperty("Accept", "application/json");
            System.out.println("coucou API");
            if (connect.getResponseCode() != 200) {
                LOG.warning("Erreur de retour ( != 200 )");
                return null;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder retour = new StringBuilder();
            String input;
            while ((input = in.readLine()) != null) {
                retour.append(input);
            }
            in.close();
            return Document.parse(String.valueOf(retour));
        } catch (IOException e) {
            LOG.warning(e.getMessage());
        }
        return null;
    }


    public Document connectionAPITopGen(String methode) {
        try {
            URL url = new URL(APIUrl + "?method=" + methode + "&api_key=" + this.APIKey + "&limit=10&format=json");
            HttpURLConnection connect = (HttpURLConnection) url.openConnection();
            connect.setRequestMethod("GET");
            connect.setRequestProperty("Accept", "application/json");
            System.out.println("coucou API");
            if (connect.getResponseCode() != 200) {
                LOG.warning("Erreur de retour ( != 200 )");
                return null;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder retour = new StringBuilder();
            String input;
            while ((input = in.readLine()) != null) {
                retour.append(input);
            }
            in.close();
            return Document.parse(String.valueOf(retour));
        } catch (IOException e) {
            LOG.warning(e.getMessage());
        }
        return null;
    }


}
