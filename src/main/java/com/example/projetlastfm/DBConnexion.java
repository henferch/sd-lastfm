package com.example.projetlastfm;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class DBConnexion {
    private MongoClient mongoClient;
    private static DBConnexion instance;

    DBConnexion() {
        try {
            this.mongoClient = MongoClients.create();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static DBConnexion getInstance() {
        if (instance == null) {
            return new DBConnexion();
        }
        try {
            if (instance.mongoClient != null) {
                return new DBConnexion();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return instance;
    }

    public static MongoCollection<Document> getCollection(String collectionName) {
        return getInstance().mongoClient.getDatabase("projetLastFM").getCollection("lastFM_"+collectionName);
    }

}

