package com.example.projetlastfm;

import org.bson.BsonDocument;
import org.bson.Document;

import java.util.ArrayList;

public class Morceau {
        public String nom;
        public String url;
        public String artiste;
        public int auditeurs;
        public int ecoutes;
        ArrayList<Avis> avis = new ArrayList<>();


        public Morceau(Document doc, boolean typeCrea) {
            //Sil s'agit d'une création de morceau via une requete API
            if(!typeCrea) {
                BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("track");
                this.nom = bsonDoc.get("name").asString().getValue();
                this.url = bsonDoc.get("url").asString().getValue();
                BsonDocument docArtist = (BsonDocument) bsonDoc.toBsonDocument().get("artist");
                this.artiste = docArtist.get("name").asString().getValue();
                this.auditeurs = Integer.parseInt(bsonDoc.get("listeners").asString().getValue());
                this.ecoutes = Integer.parseInt(bsonDoc.get("playcount").asString().getValue());
            }
            //sinon par une requete BDD
            else{
                BsonDocument bsonDoc = doc.toBsonDocument();
                this.nom = bsonDoc.get("nom").asString().getValue();
                this.url = bsonDoc.get("url").asString().getValue();
                this.artiste = bsonDoc.get("artiste").asString().getValue();
                this.auditeurs = bsonDoc.get("auditeurs").asInt32().getValue();
                this.ecoutes = bsonDoc.get("ecoutes").asInt32().getValue();
            }
        }

    public void addAvis(ArrayList<Document> doc){
        for(Document docUnique : doc){
            this.avis.add(new Avis(docUnique, true));
        }
    }


        public String toString(){
            String sortieAvis="";
            for (Avis avis : this.avis) {
                sortieAvis+=avis.nomUser+" : "+avis.commentaire+" ( "+avis.note+"/10)\n";
            }
            return " --- " + this.nom + " --- " +
                    "\n\nArtiste : " + this.artiste +
                    "\nAuditeurs : " + this.auditeurs +
                    "\nEcoutes : " + this.ecoutes +
                    "\nUrl LASTFM : " + this.url +
                    "\n\n--------------Avis--------------\n" +sortieAvis;
        }
}
