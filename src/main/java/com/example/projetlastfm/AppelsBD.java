package com.example.projetlastfm;


import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.mongodb.client.model.Filters.*;

public class AppelsBD {
    DBConnexion connexionBD = new DBConnexion();
    MongoCollection<Document> collectionAuth = connexionBD.getCollection("Auth");
    MongoCollection<Document> collectionArtiste = connexionBD.getCollection("Artiste");
    MongoCollection<Document> collectionTag = connexionBD.getCollection("Tag");
    MongoCollection<Document> collectionTrack = connexionBD.getCollection("Track");
    MongoCollection<Document> collectionAlbum = connexionBD.getCollection("Album");
    MongoCollection<Document> collectionAvis = connexionBD.getCollection("Avis");


// --------------------------------------------- Recherche USER ---------------------------------------------

    public boolean connexion(User user, String inputPseudo, String inputMDP) {
        try{
            Document doc = collectionAuth.find(and(eq("name", inputPseudo), eq("mdp", inputMDP))).first();
            user.actuUser(doc);
            return true;
        } catch(Exception e){return false;}

    }


    public boolean inscription(User user, String inputPseudo, String inputMDP) {
        try{
            //verifie s'il existe deja un speudo identique
            Document doc = collectionAuth.find(eq("name", inputPseudo)).first();
            try{
                doc.get("name");
                return false;
            }
            catch(Exception e){
                //création et ajout
                Map<String, Object> entree = new HashMap<>();
                entree.put("name", inputPseudo);
                entree.put("role", "user");
                entree.put("mdp", inputMDP);
                collectionAuth.insertOne(new Document(entree));
                System.out.println("Inscription faite dans BDD");
                user.actuUser(collectionAuth.find(and(eq("name", inputPseudo), eq("mdp", inputMDP))).first());
                return true;
            }
        }catch(Exception e){return false;}
    }

// --------------------------------------------- Recherche ARTISTE ---------------------------------------------

    public boolean rechercheArtiste(String nomArtiste){
        try{
            Document doc = collectionArtiste.find(eq("nom", nomArtiste)).first();
            String test = String.valueOf(doc.get("_id"));
            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public Artiste getArtiste(String nomArtiste){
        Document doc = collectionArtiste.find(eq("nom", nomArtiste)).first();
        return new Artiste(doc, true);
    }

    public void addArtiste(Artiste artiste){
        //nom auditeurs ecoutes url tag resume
        //'{nom:"Kikesa", auditeurs:10, ecoutes:10, url:"http//pop", tag:"pop", resume:"ceci est un resume"}'
        Map<String, Object> entree = new HashMap<>();
        entree.put("nom", artiste.nom);
        entree.put("auditeurs", artiste.auditeurs);
        entree.put("ecoutes", artiste.ecoutes);
        entree.put("url", artiste.urlLastFM);
        entree.put("tag", artiste.tags);
        entree.put("resume", artiste.resume);
        collectionArtiste.insertOne(new Document(entree));
        System.out.println("Artiste ajouté à la BDD");
    }

// --------------------------------------------- Recherche TAG ---------------------------------------------

    public boolean rechercheTag(String nomTag){
        try{
            Document doc = collectionTag.find(eq("nom", nomTag)).first();
            String test = String.valueOf(doc.get("_id"));
            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public Tag getTag(String nomTag){
        Document doc = collectionTag.find(eq("nom", nomTag)).first();
        return new Tag(doc, true);
    }

    public void addTag(Tag tag){
        Map<String, Object> entree = new HashMap<>();
        entree.put("nom", tag.nom);
        entree.put("portee", tag.portee);
        entree.put("nombre", tag.nombre);
        entree.put("resume", tag.resume);
        collectionTag.insertOne(new Document(entree));
        System.out.println("Tag ajouté à la BDD");
    }

// --------------------------------------------- Recherche MORCEAU ---------------------------------------------

    public boolean rechercheMorceau(String nomMorceau, String nomArtiste){
        try{
            //exemple = db.lastFM_Track.find({$and:[{nom:"Poudre dodo"},{artiste:"KIKESA"}]});
            Document doc = collectionTrack.find(and(eq("nom", nomMorceau), eq("artiste", nomArtiste))).first();
            String test = String.valueOf(doc.get("_id"));
            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public Morceau getMorceau(String nomMorceau, String nomArtiste){
        Document doc = collectionTrack.find(and(eq("nom", nomMorceau), eq("artiste", nomArtiste))).first();
        Morceau morceau = new Morceau(doc, true);
        return morceau;
    }

    public void addMorceau(Morceau morceau){
        Map<String, Object> entree = new HashMap<>();
        entree.put("nom", morceau.nom);
        entree.put("url", morceau.url);
        entree.put("artiste", morceau.artiste);
        entree.put("auditeurs", morceau.auditeurs);
        entree.put("ecoutes", morceau.ecoutes);
        collectionTrack.insertOne(new Document(entree));
        System.out.println("Morceau ajouté à la BDD");
    }

// --------------------------------------------- Recherche ALBUM ---------------------------------------------

    public boolean rechercheAlbum(String nomAlbum, String nomArtiste){
        try{
            Document doc = collectionAlbum.find(eq("nom", nomAlbum)).first();
            String test = String.valueOf(doc.get("_id"));
            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public Album getAlbum(String nomAlbum, String nomArtiste){
        Document doc = collectionAlbum.find(and(eq("nom", nomAlbum), eq("artiste", nomArtiste))).first();
        return new Album(doc, true);
    }

// --------------------------------------------- Recherche Avis ---------------------------------------------

    public ArrayList<Document> getAvis(String objet, String nomArtiste){
        try{
            ArrayList<Document> listDocs = new ArrayList<>();
            FindIterable<Document> docs = collectionAvis.find((and(eq("nomObjet", objet), eq("nomArtiste", nomArtiste))));

            for(Document doc : docs) {
                listDocs.add(doc);
            }
            return listDocs;
        } catch(Exception e){
            System.out.println("erreur :" + e);
            return new ArrayList<Document>();
        }
    }


    public Document getAvisGen(){
        try{
            int i = (int) collectionAvis.countDocuments((Bson) collectionAvis.find(eq("etatValidation", false)));
            Document doc = (Document) collectionAvis.find(eq("etatValidation", false));
            return doc;
        } catch(Exception e){return new Document();}
    }

    public void addAvis(Avis avis){
        Map<String, Object> entree = new HashMap<>();
        entree.put("nomObjet", avis.nomObjet);
        entree.put("nomArtiste", avis.nomArtiste);
        entree.put("nomUser", avis.nomUser);
        entree.put("note", avis.note);
        entree.put("commentaire", avis.commentaire);
        entree.put("etatValidation", false);
        collectionAvis.insertOne(new Document(entree));
        System.out.println("Avis ajouté à la BDD");
    }


}
