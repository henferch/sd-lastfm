package com.example.projetlastfm;

import org.bson.Document;


public class AppelsAPI {
    ConnectAPI connect = new ConnectAPI();

    public Document searchArtiste(String artiste) {
        Document doc = connect.connectionAPI("artist.search", ("artist=" + artiste));
        return doc;
    }

    public Artiste getArtiste(String artiste) {
        Document doc = connect.connectionAPI("artist.getInfo", ("artist=" + artiste));
        if(!doc.isEmpty()){
            Artiste artisteCree = new Artiste(doc, false);
            //Document docTag = connect.connectionAPI("artist.getTags", ("artist=" + artiste));
            //artisteCree.setTag(docTag);
            return artisteCree;
        }
        return null;
    }

    public Document getTopAlbumArtiste(String artiste) {
        Document doc = connect.connectionAPI("artist.gettopalbums", ("artist=" + artiste));
        return doc;
    }

    public Tag getTag(String tag) {
        Document doc = connect.connectionAPI("tag.getInfo", ("tag=" + tag));
        if(!doc.isEmpty()){
            return new Tag(doc, false);
        }
        return null;
    }


    public Morceau getMorceau(String morceau, String artiste) {
        Document doc = connect.connectionAPI2("track.getInfo", ("artist="+artiste+"&track=" + morceau));
        if(!doc.isEmpty()){
            return new Morceau(doc, false);
        }
        return null;
    }


    public Album getAlbum(String album, String artiste) {
        Document doc = connect.connectionAPI2("album.getInfo", ("artist="+artiste+"&album=" + album));
        if(!doc.isEmpty()){
            return new Album(doc,false);
        }
        return null;
    }


    public TopGenArtiste getTopGenArtiste(){
        Document doc = connect.connectionAPITopGen("chart.gettopartists");
        if(!doc.isEmpty()){
            return new TopGenArtiste(doc);
        }
        return null;
    }


    public TopGenTrack getTopGenTrack(){
        Document doc = connect.connectionAPITopGen("chart.gettoptracks");
        if(!doc.isEmpty()){
            return new TopGenTrack(doc);
        }
        return null;
    }


    public TopGenTag getTopGenTag(){
        Document doc = connect.connectionAPITopGen("chart.gettoptags");
        if(!doc.isEmpty()){
            return new TopGenTag(doc);
        }
        return null;
    }

    public TopGeoTrack getTopGeoTrack(String inputPays){
        Document doc = connect.connectionAPITopGen("geo.gettoptracks&country="+inputPays);
        if(!doc.isEmpty()){
            return new TopGeoTrack(inputPays, doc);
        }
        return null;
    }


    public TopGeoArtist getTopGeoArtist(String inputPays){
        Document doc = connect.connectionAPITopGen("geo.gettopartists&country="+inputPays);
        if(!doc.isEmpty()){
            return new TopGeoArtist(inputPays, doc);
        }
        return null;
    }
}
